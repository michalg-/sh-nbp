# README

Application allows to check rates for USD and EUR for any bank date since 02.01.2019, check historical data from defined range and to check average currency rates for defined time range.

To obtain data request needs to be authorized.
To authorize pass in headers:
```
  X-ACCESS-TOKEN: 1a28cc2a-fa10-4dfe-bc92-e8f397174085
```
Token is defined in secrets.yml file, which is present on repo intentionally.

##Obtaining rates for selected day:
```
  GET /api/v1/days/DATE
```
DATE - date formated in iso8601

---
###Example result:
```json
{
    "date": "2019-06-11",
    "currencies": [
        {
            "code": "USD",
            "rate": "3.7722"
        },
        {
            "code": "EUR",
            "rate": "4.2675"
        }
    ]
}
```

##Obtaining rates for date range:
```
  GET /api/v1/days?start_date=START_DATE&end_date=END_DATE
```
START_DATE - date formated in iso8601
END_DATE - date formated in iso8601


---
###Example result:
```json
[
  {
    "date": "2019-06-12",
    "currencies": [
      {
          "code": "USD",
          "rate": "3.763"
      },
      {
          "code": "EUR",
          "rate": "4.265"
      }
    ]
  },
  {
    "date": "2019-06-11",
    "currencies": [
      {
          "code": "USD",
          "rate": "3.7722"
      },
      {
          "code": "EUR",
          "rate": "4.2675"
      }
    ]
  },
  ...
]
```

##Obtaining average currency rates for date range:

```
  GET /api/v1/days/average/?start_date=START_DATE&end_date=END_DATE
```

START_DATE - start date formated in iso8601

END_DATE - end date formated in iso8601

---
###Example result:
```json
{
    "USD": "3.8282047619047619",
    "EUR": "4.2905857142857143"
}
```

###Errors
API returns message, if any hight level error occurs
```json
{
  "error": "Wrong date range"
}
```

##Limitation
Currently API allows to check average USD and EUR prices in range of 93 days.
API saves results for old requests and checks rates daily, so that range will be incresed day after the widest request.

Selected days currencies are limited to NBP historical data and API cannot predict prices in the future.

API has throttling suupport to avoid too many requests from single IP.
