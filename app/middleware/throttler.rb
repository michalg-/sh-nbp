class Throttler
  REQUESTS_PER_MINUTE = 10

  def initialize(app)
    @app = app
  end

  def call(env)
    remote_ip = env['REMOTE_HOST']

    exisitng = redis.get(remote_ip)

    redis.setex(remote_ip, 60, 0) unless exisitng

    if redis.incr(remote_ip) > REQUESTS_PER_MINUTE
      content = { error: 'Too many requests' }.to_json
      return [503, {'Content-Type' => 'application/json', 'Content-Length' => content.size.to_s}, [content]]
    end

    app.call(env)
  end

  private
  attr_reader :app

  def redis
    @redis ||= Redis.new
  end
end
