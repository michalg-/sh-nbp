module DayRepo
  module_function

  def find_by_date(date)
    retries = 0

    begin
      Day.find_by!(date: date)
    rescue ActiveRecord::RecordNotFound

      retries += 1

      if retries > 1
        raise
      else
        CurrencyFetch.fetch(date)
        retry
      end
    end
  end
end
