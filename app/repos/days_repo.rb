class DaysRepo
  DatesMissing = Class.new(StandardError)

  def initialize(start_date: nil, end_date: nil)
    raise DatesMissing unless start_date.present? && end_date.present?

    @start_date = Date.parse(start_date)
    @end_date = Date.parse(end_date)
  end

  def find_in_date_range
    fetch_missing_days

    Day.where(
      'date >= :start_date AND days.date <= :end_date',
      start_date: start_date,
      end_date: end_date
    ).order(date: :desc).includes(:currencies)
  end

  def find_average_in_date_range
    fetch_missing_days

    Currency.joins(:day)
      .where(
        'days.date >= :start_date AND days.date <= :end_date',
        start_date: start_date,
        end_date: end_date
      ).group(:code).average(:rate)
  end

  private
  attr_reader :start_date, :end_date

  def fetch_missing_days
    is_weekend = -> (date) do
      date.saturday? || date.sunday? || Holidays.on(date, :pl).present?
    end

    range = (start_date..end_date).to_a
    dates = range.reject(&is_weekend)

    missing_days = Array.wrap(dates) - Day.where(date: dates).pluck(:date)
    if missing_days.present?
      CurrencyFetch.fetch_range(start_date, end_date)
    end
  end
end
