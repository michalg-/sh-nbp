class DailyCurrencyFetchWorker
  include Sidekiq::Worker

  def perform
    CurrencyFetch.fetch
  end
end
