module DayCurrencies
  class Create
    def self.save(data)
      date = Day.new(date: data[:date])
      date.currencies.new(data[:currencies])

      date.save
    end
  end
end
