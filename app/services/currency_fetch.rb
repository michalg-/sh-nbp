class CurrencyFetch
  def self.fetch(date = Date.current.iso8601)
    return if Day.exists?(date: date)

    data = Nbp::Api.fetch(date)

    DayCurrencies::Create.save(data[0])
  end

  def self.fetch_range(start_date, end_date)
    data = Nbp::Api.fetch_range(start_date, end_date)

    data.each do |datum|
      DayCurrencies::Create.save(datum)
    end
  end
end
