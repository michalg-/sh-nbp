module Api
  module V1
    module Days
      class AveragesController < ApiController
        def show
          render json: average
        rescue Nbp::Api::RequestError => e
          render json: { error: e.message }, status: e.error_code
        rescue DaysRepo::DatesMissing
          render json: { error: I18n.t('api.response.error.dates_missing') }, status: 400
        end

        private

        def average
          @average ||= days_repo.find_average_in_date_range
        end

        def days_repo
          @days_repo ||= DaysRepo.new(**range_params)
        end

        def range_params
          params.permit(:start_date, :end_date).symbolize_keys
        end
      end
    end
  end
end
