module Api
  module V1
    class ApiController < ApplicationController
      before_action :authenticate

      protected

      def authenticate
        unless request.headers['X-ACCESS-TOKEN'] == Rails.application.secrets.dig(:api, 'token')
          render json: {}, status: :unauthorized
        end
      end

    end
  end
end
