module Api
  module V1
    class DaysController < ApiController

      def index
        render :index, locals: { days: days }
      rescue Nbp::Api::RequestError => e
        render json: { error: e.message }, status: e.error_code
      rescue DaysRepo::DatesMissing
        render json: { error: I18n.t('api.response.error.dates_missing') }, status: 400
      end

      def show
        render :show, locals: { day: day }
      rescue Nbp::Api::RequestError => e
        render json: { error: e.message }, status: e.error_code
      end

      private

      def day
        @day ||= DayRepo.find_by_date(params[:date])
      end

      def days_repo
        @days_repo ||= DaysRepo.new(**index_params)
      end

      def days
        @days ||= days_repo.find_in_date_range
      end

      def index_params
        params.permit(:start_date, :end_date).symbolize_keys
      end
    end
  end
end
