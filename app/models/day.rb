class Day < ActiveRecord::Base
  has_many :currencies, dependent: :destroy

  validates :date, uniqueness: true
  validates :date, presence: true
end
