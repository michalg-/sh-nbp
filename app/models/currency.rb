class Currency < ActiveRecord::Base
  belongs_to :day

  validates :rate, :code, presence: true
  validates :code, uniqueness: { scope: :day_id }

  validates :code, inclusion: { in: %w(USD EUR) }
end
