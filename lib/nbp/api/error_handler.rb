module Nbp
  class Api
    module ErrorHandler
      module_function

      def handle(response)
        case response
        when Net::HTTPBadRequest
          raise Nbp::Api::RequestError.new(message(response.message), response.code)
        when Net::HTTPNotFound
          raise Nbp::Api::RequestError.new(I18n.t('api.response.error.not_found'), response.code)
        else
          raise Nbp::Api::RequestError.new(I18n.t('api.response.error.general'), response.code)
        end
      end

      def message(message)
        message = message.encode('UTF-8', 'binary', invalid: :replace, undef: :replace, replace: '')
        case message
        when /Bdny zakres dat/ then I18n.t('api.response.error.wrong_date_range')
        when /Przekroczony limit 93 dni/ then I18n.t('api.response.error.max_93_days')
        else I18n.t('api.response.error.general')
        end
      end
    end
  end
end
