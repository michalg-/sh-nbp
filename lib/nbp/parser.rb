module Nbp
  class Parser
    def initialize(response)
      @response = JSON.parse(response)
    end

    def parse
      response.map do |elem|
        {
          date: elem['effectiveDate'],
          currencies: [
            {
              code: 'USD',
              rate: rates_fetcher(elem).fetch('USD')
            },
            {
              code: 'EUR',
              rate: rates_fetcher(elem).fetch('EUR')
            }
          ]
        }
      end
    end

    private
    attr_reader :response

    def rates_fetcher(elem)
      Nbp::Parser::RateFetcher.new(elem['rates'])
    end
  end
end
