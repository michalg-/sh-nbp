module Nbp
  class Parser
    class RateFetcher
      def initialize(rates)
        @rates = rates
      end

      def fetch(rate_code)
        rate = rates.find { |elem| elem['code'] == rate_code }

        return nil unless rate

        rate['mid']
      end

      private
      attr_reader :rates
    end
  end
end
