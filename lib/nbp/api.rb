require 'net/http'

module Nbp
  class Api
    def self.fetch(date)
      new.fetch(date)
    end

    def self.fetch_range(start_date, end_date)
      new.fetch_range(start_date, end_date)
    end

    def initialize
      @url = URI("http://api.nbp.pl/api/exchangerates/tables/A/")
      url.query = URI.encode_www_form(format: :json)
    end

    def fetch(date)
      url.path += date

      Nbp::Parser.new(response).parse
    end

    def fetch_range(start_date, end_date)
      url.path += [start_date, end_date].join('/')

      Nbp::Parser.new(response).parse
    end

    private
    attr_reader :url

    def response
      response = Net::HTTP.get_response(url)

      return response.body if response.code == '200'

      Nbp::Api::ErrorHandler.handle(response)
    end
  end
end
