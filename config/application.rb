require File.expand_path('../boot', __FILE__)

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Nbp
  class Application < Rails::Application
    config.autoload_paths += [
      "#{config.root}/lib",
      "#{config.root}/app/middleware",
      "#{config.root}/app/repos",
      "#{config.root}/app/services",
      "#{config.root}/app/workers"
    ]
    config.active_record.raise_in_transactional_callbacks = true

    config.middleware.use 'Throttler'

    if defined?(Rails::Server)
      config.after_initialize do
        DailyCurrencyFetchWorker.perform_async
      end
    end
  end
end
