Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      namespace :days do
        resource :average, only: [:show]
      end
      resources :days, only: [:index, :show], param: :date
    end
  end
end
