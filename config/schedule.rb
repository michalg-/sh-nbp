set :output, 'log/schedule.log'

every 1.day, at: '8:17 pm' do
  runner 'DailyCurrencyFetchWorker.perform_async'
end
