require 'spec_helper'

RSpec.describe DayRepo do
  describe '.find_by_date' do
    subject { described_class.find_by_date(date) }
    let(:date) { Date.current }

    context 'when day with date present' do
      let!(:day) { Day.create(date: date) }

      it 'returns day' do
        expect(subject).to eq(day)
      end
    end

    context "when day with date doesn't present" do
      before do
        allow(CurrencyFetch).to receive(:fetch).with(date)
      end

      it 'calls CurrencyFetch.fetch' do
        expect(CurrencyFetch).to receive(:fetch).with(date).once

        expect { subject }.to raise_error
      end

      it 'calls Day.find_by! twice' do
        expect(Day).to receive(:find_by!).with(date: date).twice.and_call_original

        expect { subject }.to raise_error
      end
    end
  end
end
