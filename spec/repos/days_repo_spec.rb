require 'spec_helper'

RSpec.describe DaysRepo do
  subject(:described_object) { described_class.new(date_params) }

  describe '.find_average_in_date_range' do
    subject { described_object.find_average_in_date_range }
    before do
      allow(CurrencyFetch).to receive(:fetch_range)
    end

    context 'if missing_day is sunday' do
      let(:date_params) do
        {
          start_date: '2019-06-09',
          end_date: '2019-06-12'
        }
      end

      before do
        ['2019-06-10', '2019-06-11'].each.with_index(2) do |date, i|
          day = Day.new(date: date)
          day.currencies.new([{code: 'USD', rate: i}, {code: 'EUR', rate: i}])
          day.save!
        end
      end

      it 'call api fetch' do
        expect(CurrencyFetch).to receive(:fetch_range)
        subject
      end

      it 'returns average value' do
        expect(subject).to eq({
          "EUR" => 2.5,
          "USD" => 2.5
        })
      end
    end
  end

  describe '.find_in_date_range' do
    subject { described_object.find_average_in_date_range }
    before do
      allow(CurrencyFetch).to receive(:fetch_range)
    end

    context 'if missing_day is sunday' do
      let(:date_params) do
        {
          start_date: '2019-06-09',
          end_date: '2019-06-12'
        }
      end

      before do
        ['2019-06-10', '2019-06-11'].each.with_index(2) do |date, i|
          day = Day.new(date: date)
          day.currencies.new([{code: 'USD', rate: i}, {code: 'EUR', rate: i}])
          day.save!
        end
      end

      it 'call api fetch' do
        expect(CurrencyFetch).to receive(:fetch_range)
        subject
      end

      it 'returns exisiting records' do
        expect(subject.size).to eq(2)
      end
    end
  end
end
