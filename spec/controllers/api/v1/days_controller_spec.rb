require 'spec_helper'
require './spec/shared/api/v1/api_controller_spec.rb'

RSpec.describe Api::V1::DaysController, type: :request do
  let(:headers) { { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' } }
  let(:date) { Date.current.iso8601 }

  it_behaves_like "Unauthorized when token doesn't match"

  it 'lists requested days' do
    current_date = Date.current

    2.times do |i|
      day = Day.new(date: current_date + i.day)
      day.currencies.new([{code: 'USD', rate: 999}, {code: 'EUR', rate: 888}])
      day.save
    end


    get "/api/v1/days/?start_date=#{current_date}&end_date=#{current_date + 2.days}", {}, headers

    expect(JSON.parse(response.body).size).to eq(2)
  end

  it 'shows requested day with currencies' do
    day = Day.new(date: date)
    day.currencies.new([{code: 'USD', rate: 999}, {code: 'EUR', rate: 888}])
    day.save

    get "/api/v1/days/#{date}", {}, headers

    expect(JSON.parse(response.body)).to eq({
      "date" => date,
      "currencies" => [{"code"=>"USD", "rate"=>"999.0"}, {"code"=>"EUR", "rate"=>"888.0"}]
    })
  end

  it 'API raises error' do
    allow(DayRepo).to receive(:find_by_date).and_raise(Nbp::Api::RequestError.new('some error', 500))

    get "/api/v1/days/2019-03-12", {}, headers
    expect(JSON.parse(response.body)).to eq('error' => 'some error')
    expect(response.code).to eq('500')
  end
end
