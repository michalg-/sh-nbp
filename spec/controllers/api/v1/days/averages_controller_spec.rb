require 'spec_helper'
require './spec/shared/api/v1/api_controller_spec.rb'

RSpec.describe Api::V1::Days::AveragesController, type: :request do
  let(:headers) { { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' } }
  let(:params) { { start_date: Date.current.iso8601, end_date: (Date.current + 1).iso8601 } }
  let(:days_repo) { instance_double(DaysRepo, find_average_in_date_range: double) }
  before do
    allow(DaysRepo).to receive(:new).and_return(days_repo)
  end

  it_behaves_like "Unauthorized when token doesn't match"

  it 'shows average rate in passed days' do

    allow(days_repo).to receive(:find_average_in_date_range).and_return(
      {
        'EUR' => '3.0',
        'USD' => '3.0'
      }.to_json
    )

    get "/api/v1/days/average", params, headers

    expect(JSON.parse(response.body)).to eq(
      'EUR' => '3.0',
      'USD' => '3.0'
    )
  end

  it 'API raises error' do
    allow(days_repo).to receive(:find_average_in_date_range).and_raise(Nbp::Api::RequestError.new('some error', 500))

    get "/api/v1/days/average", params, headers
    expect(JSON.parse(response.body)).to eq('error' => 'some error')
    expect(response.code).to eq('500')
  end
end
