require 'spec_helper'

RSpec.describe Nbp::Api::RequestError do
  subject(:described_object) { described_class.new('some strange error', 400) }

  describe '#error_code' do
    subject { described_object.error_code }
    it { expect(subject).to eq(400) }
  end

  describe '#message' do
    subject { described_object.message }
    it { expect(subject).to eq('some strange error') }
  end
end
