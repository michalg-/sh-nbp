require 'spec_helper'

RSpec.describe Nbp::Api::ErrorHandler do
  subject { described_class.handle(response) }

  describe 'undefined Net error' do
    let(:response) { Net::HTTPRequestTimeOut.new(1, 504, 'Timeout') }
    it 'assigns message and code to Nbp::Api::RequestError' do
      expect(Nbp::Api::RequestError).to receive(:new).with('API communication error occured', 504)
      expect { subject }.to raise_error
    end
  end

  describe 'Net::HTTPBadRequest' do
    context 'wrong dates range' do
      let(:response) { Net::HTTPBadRequest.new(1, 400, 'Bdny zakres dat') }
      it 'assigns message and code to Nbp::Api::RequestError' do
        expect(Nbp::Api::RequestError).to receive(:new).with('Wrong date range', 400)
        expect { subject }.to raise_error
      end
    end

    context '93 days limit exceeded' do
      let(:response) { Net::HTTPBadRequest.new(1, 400, 'Przekroczony limit 93 dni') }
      it 'assigns message and code to Nbp::Api::RequestError' do
        expect(Nbp::Api::RequestError).to receive(:new).with('93 days range exceeded', 400)
        expect { subject }.to raise_error
      end
    end
  end

  describe 'Net::HTTPNotFound' do
    let(:response) { Net::HTTPNotFound.new(1, 404, 'Not Found - Brak danych') }

    it 'assigns message and code to Nbp::Api::RequestError' do
      expect(Nbp::Api::RequestError).to receive(:new).with('Currency rates for selected day not exists', 404)
      expect { subject }.to raise_error
    end
  end
end
