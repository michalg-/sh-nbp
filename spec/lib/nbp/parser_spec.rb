require 'spec_helper'

RSpec.describe Nbp::Parser do
  subject(:described_object) { Nbp::Parser.new(response) }

  let(:response) do
    [
      {
        effectiveDate: '2020-04-16',
        rates: [
          {
            currency: 'dolar amerykański',
            code: 'USD',
            mid: 3.7722
          },
          {
            currency: 'euro',
            code: 'EUR',
            mid: 4.2675
          }
        ]
      }
    ].to_json
  end

  describe '#parse' do
    subject { described_object.parse }

    it 'returns date' do
      expect(subject[0][:date]).to eq('2020-04-16')
    end

    it 'returns currencies' do
      expect(subject[0][:currencies]).to eq([
        {
          code: 'USD',
          rate: 3.7722
        },
        {
          code: 'EUR',
          rate: 4.2675
        }
      ])
    end
  end
end
