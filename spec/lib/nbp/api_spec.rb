require 'spec_helper'

RSpec.describe Nbp::Api do
  subject { described_class.fetch(date) }

  let(:date) { Date.current.iso8601 }

  let(:uri) { URI("http://api.nbp.pl/api/exchangerates/tables/A/#{date}?format=json") }
  let(:parser_instance) { instance_double(Nbp::Parser, parse: double) }

  let(:response_double) { instance_double(Net::HTTPOK, code: '200', body: '{}') }

  before do
    allow(Net::HTTP).to receive(:get_response).and_return(response_double)
    allow(Nbp::Parser).to receive(:new).with('{}').and_return(parser_instance)
  end

  it 'calls NBP API' do
    expect(Net::HTTP).to receive(:get_response).with(uri)
    subject
  end

  it 'calls NBP api' do
    expect(parser_instance).to receive(:parse)
    subject
  end
end
