require 'spec_helper'

RSpec.describe Nbp::Parser::RateFetcher do
  subject(:described_object) { described_class.new(rates) }

  let(:rates) do
    [
      {
        'currency' => 'dolar amerykański',
        'code' => 'USD',
        'mid' => 3.7722
      },
      {
        'currency' => 'euro',
        'code' => 'EUR',
        'mid' => 4.2675
      }
    ]
  end

  describe '#fetch' do
    subject { described_object.fetch(rate_code) }

    let(:rate_code) { 'USD' }

    it 'returns rate' do
      expect(subject).to eq(3.7722)
    end
  end
end
