require 'spec_helper'

RSpec.describe Throttler do
  subject(:described_object) { described_class.new(app) }

  let(:app) { double(call: double) }
  let(:env) { { 'REMOTE_HOST' => '1234' } }

  describe '#call' do
    subject { described_object.call(env) }

    let(:redis_insance) do
      instance_double(
        Redis,
        get: false,
        setex: double,
        incr: double(:>= => double)
      )
    end

    before do
      allow(Redis).to receive(:new).and_return(redis_insance)
    end

    context 'on first visit' do
      before do
        allow(redis_insance).to(receive(:incr).with(env['REMOTE_HOST'])).and_return(1)
      end

      it 'check object with IP key presence' do
        expect(redis_insance).to receive(:get).with(env['REMOTE_HOST']).and_return(false)
        subject
      end

      it 'adds redis record with TTL' do
        expect(redis_insance).to(receive(:setex).with(env['REMOTE_HOST'], 60, 0))
        subject
      end

      it 'increment requests count' do
        expect(redis_insance).to(receive(:incr).with(env['REMOTE_HOST'])).and_return(1)
        subject
      end

      it 'calls app' do
        expect(app).to(receive(:call).with(env))
        subject
      end
    end

    context 'on eleven visit' do

      before do
        allow(redis_insance).to(receive(:incr).with(env['REMOTE_HOST'])).and_return(11)
      end

      it 'check object with IP key presence' do
        expect(redis_insance).to receive(:get).with(env['REMOTE_HOST']).and_return(true)
        subject
      end

      it 'returns 503 response' do
        expect(subject).to eq([503, {'Content-Length'=>'29', 'Content-Type'=>'application/json'}, ['{"error":"Too many requests"}']])
      end

      it 'calls app' do
        expect(app).not_to receive(:call)
        subject
      end
    end

  end
end
