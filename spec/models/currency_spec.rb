require 'spec_helper'

RSpec.describe Currency, type: :model do
  it { should validate_presence_of(:rate) }
  it { should validate_presence_of(:code) }
  it { should belong_to(:day) }
  it { should validate_uniqueness_of(:code).scoped_to(:day_id) }
  it { should validate_inclusion_of(:code).in_array(%w(USD EUR)) }
end
