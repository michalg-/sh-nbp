require 'spec_helper'

RSpec.describe Day, type: :model do
  it { should validate_presence_of(:date) }
  it { should validate_uniqueness_of(:date) }
  it { should have_many(:currencies).dependent(:destroy) }
end
