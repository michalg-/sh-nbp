require 'spec_helper'

RSpec.describe DayCurrencies::Create do
  subject { described_class.save(data) }
  let(:data) do
    {
      date: Date.current,
      currencies: [
        {
          code: 'USD',
          rate: 99.99
        },
        {
          code: 'EUR',
          rate: 1.11
        }
      ]
    }
  end

  describe '.save' do
    context 'valid data' do
      it 'saves day' do
        expect { subject }.to change(Day, :count).by(1)
      end

      it 'saves currencies' do
        expect { subject }.to change(Currency, :count).by(2)
      end
    end

    context 'invalid data' do
      context 'date is blank' do
        before do
          data[:date] = nil
        end

        it "doesn't save day" do
          expect { subject }.not_to change(Day, :count)
        end

        it "doesn't save currencies" do
          expect { subject }.not_to change(Currency, :count)
        end
      end

      context 'currency is invalid' do
        before do
          data[:currencies][0][:code] = nil
        end

        it "doesn't save day" do
          expect { subject }.not_to change(Day, :count)
        end

        it "doesn't save currencies" do
          expect { subject }.not_to change(Currency, :count)
        end
      end
    end
  end
end
