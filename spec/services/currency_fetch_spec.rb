require 'spec_helper'

RSpec.describe CurrencyFetch do
  describe '.fetch' do
    subject { described_class.fetch(date) }

    let(:date) { Date.current }
    context 'when Day with passed date is present' do
      before do
        Day.create!(date: date)
      end

      it 'returns nil' do
        expect(subject).to eq(nil)
      end

      it "doesn't call API" do
        expect(Nbp::Api).not_to receive(:fetch)
        subject
      end
    end

    context "when Day with passed date is not present" do
      before do
        allow(DayCurrencies::Create).to receive(:save)
        allow(Nbp::Api).to receive(:fetch).with(date).and_return([exaple: :data])
      end

      it 'calls API' do
        expect(Nbp::Api).to receive(:fetch).with(date)
        subject
      end

      it 'calls DayCurrencies::Create.save' do
        expect(DayCurrencies::Create).to receive(:save).with(exaple: :data)
        subject
      end
    end
  end

  describe '.fetch_range' do
    subject { described_class.fetch_range(start_date, end_date) }
    let(:start_date) { Date.current - 1.day }
    let(:end_date) { Date.current + 1.day }

    before do
      allow(Nbp::Api).to receive(:fetch_range).with(start_date, end_date).and_return([{some: :data}])
    end

    it 'calls API for data' do
      expect(Nbp::Api).to receive(:fetch_range).with(start_date, end_date).and_return([])
      subject
    end

    it 'calls days creator' do
      expect(DayCurrencies::Create).to receive(:save).with({some: :data})
      subject
    end
  end
end
