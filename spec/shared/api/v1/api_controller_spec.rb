RSpec.shared_examples "Unauthorized when token doesn't match" do
  it 'returns unauthorized' do
    allow(Rails.application.secrets).to receive(:dig).with(:api, 'token').and_return(api: { 'token' => 1234 })

    get '/api/v1/days/2019-06-12'

    expect(response.code).to eq('401')
  end
end
