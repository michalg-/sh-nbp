class AddIndexToDayDate < ActiveRecord::Migration
  def change
    add_index :days, :date
  end
end
