class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.string :code
      t.references :day, index: true, foreign_key: true
      t.decimal :rate

      t.timestamps null: false
    end
  end
end
